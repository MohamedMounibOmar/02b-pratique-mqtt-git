# 02b Pratique MQTT-Git [+ (28pts) +]

## Objectifs

* Appliquer les concepts de fork, branche et merge request sur GitLab
* Compléter un script Python MQTT
* Effectuer un commit et un push
* Créer une branche
* Fusionner des branches

## Ressources 

* [Documentation Git](https://git-scm.com/book/fr/v2)
* [Documentation paho-mqtt](https://pypi.org/project/paho-mqtt)

## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre `<visa>` est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom
* Si vous êtes bloqué(e) dans vos manipulations git, demandez à l'enseignant. Les points des parties non réalisées seront déduites mais vous pourrez continuer.

## Installation

```bash
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
pip install paho-mqtt
```

## Instructions Git [+ (7pts) +]

### 1. Forker le projet [+ (2pt) +]

Rendez-vous sur le dépôt GitLab du projet et cliquez sur le bouton "Fork". Cela créera une copie du projet dans votre espace de travail GitLab.

### 2. Cloner le répertoire distant sur votre machine [+ (2pts) +]

- Récupérez le lien https de votre repo nouvellement créé **[+ (1pt) +]**

- Clonez ce repo sur votre ordinateur local **[+ (1pt) +]**
```bash
# TODO indiquez les commandes utilisées
```

### 3. Ajouter un fichier Python

Créez un fichier `bonjour_<visa>.py` avec le code suivant :

```python
print("<visa>")
```

### 4. Ajouter et commiter le fichier [+ (2pts) +]

Ajoutez et commitez le fichier avec le message de commit suivant : `<visa> - création de bonjour_<visa>.py`

```bash
# TODO indiquez les commandes utilisées
```

### 5. Pousser les modifications vers le dépôt distant [+ (1pt) +]

```bash
# TODO indiquez les commandes utilisées
```


## Commandes Git [+ (6pts) +]

### 1. Intilialiser le dépôt Git [+ (2pt) +]

Indiquez la commande permettant d'initilaise le répertoire courant en dépôt Git : 

```bash
# TODO indiquez les commandes utilisées
```

### 2. Consulter l'historique des commits [+ (1pt) +]

Indiquez la commande permettant de consulter l'historique: 

```bash
# TODO indiquez les commandes utilisées
```

### 3. Branche [+ (1pt) +]

Indiquez la commande permettant de créer une branche : 

```bash
# TODO indiquez les commandes utilisées
```


### 4. Statut [+ (2pt) +]

Indiquez la commande permettant de consulter le statut du dossier git : 

```bash
# TODO indiquez les commandes utilisées
```

## MQTT [+ (13pt) +]

Pour les exercices suivants, vous pouvez tester vos résultats avec MQTTX.

### 1. Paho-mqtt [+ (2pt) +]
Installez la libraire `paho-mqtt` 

```bash
# TODO indiquez les commandes utilisées
```

### 1. Modifiez les fichier `send.py` [+ (6pt) +]
- Changez le topic en `<visa>/examen-02` **[+ (2pt) +]**
- Faites en sorte que la vauleur saisie par l'utilisateur soit envoyé dans ce topic **[+ (2pts) +]**
- Si le message n'a pas été envoyé, Affichez les message suivant: `"Impossible d'envoyer le message"` **[+ (2pt) +]**


### 2. Modifiez les fichier `receive.py` [+ (5pt) +]
- Changez le topic en fonction de la valeur saisie par l'utilisateur **[+ (2pt) +]**
- Quand un message est reçu, affichez-le à l'écran (`print`) **[+ (2pts) +]**
- Quand un message est reçu, affichez également le QoS reçu **[+ (1pts) +]**

### 3. Ajouter et commiter les fichier

Ajoutez et commitez les fichiers `send.py` et `receive.py` avec le message de commit suivant : `<visa> - modifications de send.py et receive.py`

### 4. Pousser les modifications vers le dépôt distant


## Git merge [+ (2pt) +]
Sur GitLab, effectuez "Merge Requests" avec le message suivant:
```
<visa>: 02b - Travail terminé
```